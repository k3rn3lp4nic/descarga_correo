import imaplib
import email
from email import policy
import registro as reg
import sqlite3
import os
import time

@contextmanager
def adjunto2fichero(nombre, modo='w'):
    try:
        f = open(nombre, modo)
    except IOError, err:
        yield None, err
    else:
        try:
            yield f, None
        finally:
            f.close()

class Buzon():
    """Se conecta al servidor de correo mediante el protocolo IMAP y provee
    propiedades del buzón y métodos para interactuar con él
    """
    def __init__(self, usuario, clave, servidor, id):
        try:
            self.conexion = imaplib.IMAP4_SSL(servidor)
            self.conexion.login(usuario, clave)
            self.conexion.select(readonly=False) # Para poder marcar los
                                                 # correos como leídos
        except:
            reg.logger.critical(
                    f"No se ha podido logear al buzón de {id}",
                    exc_info=True
                    )

        self.id = id

    def cerrar_buzon(self):
        """Cerrar la conexión con el servidor de correo
        """
        self.conexion.close()

    def no_leidos(self) -> list:
        """Lista los correos no leídos
        """
        correos = []

        respuesta, ids = self.conexion.search(None, 'UnSeen')
        if respuesta == 'OK' and ids[0].decode() != '':
            for id in ids[0].decode().split(' '):
                try:
                    _, datos = self.conexion.fetch(id, '(RFC822)')
                except:
                    reg.logger.error(
                            f"Error al inspeccionar el buzón de {self.id}",
                            exc_info=True
                            )
                    self.cerrar_buzon()
                    continue
                correo = email.message_from_bytes(
                        datos[0][1],
                        policy=policy.default
                        )
                if not isinstance(correo, str): correos.append(correo)
        else:
            reg.logger.info('No hay correos nuevos')
            self.cerrar_buzon()

        return correos

    def guardar_estado_correo(self, correo, bbdd) -> None:
        """SQLite3 para guardar remitente, asunto, fecha de llegada y descarga
        en BBDD
        """
        estado = (
                email.utils.parseaddr(correo['from'])[1],
                email.header.decode_header(correo['Subject'])[0][0],
                time.strftime(
                    '%Y-%m-%d %H:%M:%S',
                    email.utils.parsedate(correo['Date'])
                    )
                )

        try:
            conexion_bbdd = sqlite3.connect(bbdd)
            cursor = conexion_bbdd.cursor()
            cursor.execute(
                    """INSERT INTO mail (remitente, asunto, fecha) \
                    VALUES (?, ?, ?)""", estado)
            conexion_bbdd.commmit()
            conexion_bbdd.close()
            reg.logger.info('Se ha guardado el estado del mail en BBDD')
        except:
            reg.logger.warning(
                    'No se ha podido guardar el estado del mail',
                    exc_info=True
                    )

            def obtener_adjuntos(self, correo, formatos, dir_salida, mail_id, bbdd) -> None:
                """Crea ficheros a partir de los adjuntos de un objeto mail en
                el directorio de carga (dir_salida)
                """
                conexion_bbdd = sqlite3.connect(bbdd)
                cursor = conexion_bbdd.cursor()

                for parte in correo.walk():
                    if parte.get_filename() is not None:
                        nombre = parte.get_filename()

                        if nombre.lower().endswith(formatos):
                            reg.logger.info(f"Adjunto: {nombre}")
                            ruta_fichero = os.path.join(dir_salida, nombre)
                            with adjunto2fichero(ruta_fichero) as (f, err):
                                if err:
                                    reg.logger.error(
                                            f"""No se ha podido crear el \
                                            adjunto {nombre}.\n{err}"""
                                else:
                                    f.write(parte.get_payload(decode=True))
                            try:
                                # La tabla no se puede pasar por parámetro
                                cursor.execute(
                                        f"""INSERT INTO {self.id} (nombre, mail_id) \
                                        VALUES (?, ?)""", (nombre, mail_id)
                                        )
                                conexion_bbdd.commit()
                                reg.logger.info('Guardado estado del fichero en BDD')
                            except:
                                reg.logger.warning(
                                        f"""No se ha podido guardar el estado \
                                        para el fichero {nombre} del mail (ID) \
                                        {mail_id}""",
                                        exc_info=True
                                        )
                conexion_bbdd.close()
