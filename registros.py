import logging
import requests
import yaml

with open('conf.yml', 'r') as f:
    CONFIG = yaml.safe_load(f)

class SlackHandler(logging.Handler):
    """Handler custom para alertas en Slack
    """
    def emit(self, alerta):
        log_entry = self.format(alerta)
        url = CONFIG['log']['slack_url']

# LOGGER
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# HANDLERS
alerta_slack = SlackHandler()
alerta_slack.setLevel(logging.ERROR)
registro_buzon = logging.FileHandler(CONFIG['log']['ruta'])
registro_buzon.setLevel(logging.INFO)

# FORMATTERS
fmt_slack = logging.Formatter('%(levelname)s - %(message)s')
fmt_fichero = logging.Formatter(
        '%(asctime)s: %(levelname)s - %(message)s',
        datefmt='%d-%m-%Y %H:%M:%S'
        )

alerta_slack.setFormatter(fmt_slack)
registro_buzon.setFormatter(fmt_fichero)
logger.addHandler(alerta_slack)
logger.addHandler(registro_buzon)
