#!/usr/bin/env python3

import sqlite3
import registro as reg
import buzon

def obtener_mail_id():
    conexion_bbdd = sqlite3.connect(CONFIG['bbdd'])
    cursor = conexion_bbdd.cursor()
    id = cursor.execute("SELECT MAX(id) FROM mail").fetchone()[0]
    conexion_bbdd.close()
    return int(id)

if __name__ == "__main__":
    with open('config.yml', 'r') as f:
        CONFIG = yaml.safe_load(f)
    BBDD = CONFIG['bbdd']

    for id in CONFIG['buzones']:
        reg.logger.info(f"Examinando el buzón {id}")
        buzon = buzon.Buzon(
                CONFIG['buzones'][id]['usuario'],
                CONFIG['buzones'][id]['clave'],
                CONFIG['buzones'][id]['servidor'],
                id
                )
        formatos = tuple([fmt for fmt in CONFIG['buzones'][id]['formatos']])
        correos = buzon.no_leidos()

        for correo in correos:
            reg.logger.info('Inspeccionando nuevo mail')
            buzon.guardar_estado_correo(correo, BBDD)
            mail_id = obtener_mail_id()

            buzon.obtener_adjuntos(
                    correo,
                    formatos,
                    CONFIG['buzones'][id]['dir'],
                    mail_id,
                    BBDD
                    )
